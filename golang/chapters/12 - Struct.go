package main

import "fmt"

/**
 * Struct merupakan sebuah template data
 * yang digunakan untuk menggabungkan nol atau lebih tipe data lainnya dalam satu kesatuan
 * Struct biasanya representasi data dalam program aplikasi yang kita buat
 * Data di struct disimpan dalam bentuk field
 * Sederhananya struct adalah kumpulan dari field
 */

func main() {
	type userData struct {
		firstName       string
		lastName        string
		email           string
		numberOfTickets uint
	}

	// Deklarasi variable dengan tipe data struct
	// dan inisialisasi nilai field
	user1 := userData{
		firstName:       "Jane",
		lastName:        "Doe",
		email:           "jhon@example.com",
		numberOfTickets: 2,
	}

	// Deklarasi variable dengan tipe data struct
	var user2 userData
	user2.firstName = "John"
	user2.lastName = "Doe"
	user2.email = "jhon@example.com"
	user2.numberOfTickets = 2

	fmt.Println(user1)
	fmt.Println(user2)
}
