package main

import "fmt"

func main() {
	var conferenceName = "Go Conference"
	const conferenceTicket = 100
	var remainingTickets = 100

	// default print
	//fmt.Println("Welcome to", conferenceName, "booking application")
	//fmt.Println("We have total of", conferenceTicket, "tickets and", remainingTickets, "are still available")
	//fmt.Println("Get your tickets here to attend")

	// use printf
	fmt.Printf("Welcome to %v booking application\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v are still available\n", conferenceTicket, remainingTickets)
	fmt.Println("Get your tickets here to attend")
}
