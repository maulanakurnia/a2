package main

import "fmt"

func main() {
	/** --------------------------------------------------------------
	 * if once of variable or something not used
	 * golang will show error in variable name or something not used
	 * so, if you create variable, u must use it!
	 *----------------------------------------------------------------*/
	// basic example print from Package fmt
	fmt.Print("Hello World!")
	fmt.Println("Hello World! with new line")
	fmt.Print("\nHello World! with Escape Sequence \n")
	fmt.Print("\nHello\tWorld! ")
}
