package main

import "fmt"

func main() {

	const conferenceTickets int = 100
	var remainingTickets uint = 100
	conferenceName := "Go Conference"
	var bookings []string

	fmt.Printf("Welcome to %v booking application.\nWe have total of %v tickets and %v are still available.\nGet your tickets here to attend\n", conferenceName, conferenceTickets, remainingTickets)

	var firstName string
	var lastName string
	var email string
	var userTickets uint

	fmt.Print("enter your first name\t: ")
	fmt.Scan(&firstName)

	fmt.Print("enter your last name\t: ")
	fmt.Scan(&lastName)

	fmt.Print("enter your email\t: ")
	fmt.Scan(&email)

	fmt.Print("enter number of tickets\t: ")
	fmt.Scan(&userTickets)

	remainingTickets -= userTickets
	bookings = append(bookings, firstName+" "+lastName)

	fmt.Printf("Thank you %v %v for booking %v tickets. You will receive a confirmation email at %v\n", firstName, lastName, userTickets, email)
	fmt.Printf("%v tickets remaining for %v\n", remainingTickets, conferenceName)
	fmt.Printf("These are all our bookings: %v\n", bookings)
}
