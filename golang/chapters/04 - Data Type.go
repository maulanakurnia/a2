package main

import "fmt"

func main() {
	var conferenceName string = "Go Conference"
	const conferenceTicket uint = 100
	var remainingTickets uint = 100

	// use %T for print data type
	fmt.Printf("\nconferenceTicket is %T, remainingTickets is %T, conferenceName is %T\n", conferenceTicket, remainingTickets, conferenceName)

	var userName string
	var lastName string
	var email string
	var userTickets uint

	userName = "John"
	lastName = "Doe"
	email = "example@gmail.com"
	userTickets = 10

	fmt.Printf("\nuserName is %T, lastName is %T, email is %T, userTickets is %T\n", userName, lastName, email, userTickets)
}
