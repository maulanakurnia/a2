package main

import "fmt"

/** ----------------------------------------------------------------------
  * golang have 2 variable type : var and const
  * variable var mean is a default variable and value customizable
  * but value of const can't customizable because const must be constant
  * ----------------------------------------------------------------------
  * by default, empty variable in golang is required to use data type
  * for more example data type in golang, please visit:
  * https://www.callicoder.com/golang-basic-types-operators-type-conversion/
  * -----------------------------------------------------------------------
**/

func main() {
	var conferenceName = "Go Conference" // variable var
	const conferenceTicket = 100         // const must be constant
	remainingTickets := 100              // := is a short variable declaration

	fmt.Println("Conference Name: ", conferenceName)
	fmt.Println("Conference Ticket: ", conferenceTicket)
	fmt.Println("Reaming Tickets: ", remainingTickets)

	// multiple variable declaration
	var (
		firstName string = "John"
		lastName  string = "Doe"
	)

	fmt.Println(firstName)
	fmt.Println(lastName)
}
