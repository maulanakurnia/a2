package main

import "fmt"

func main() {
	var conferenceName = "Go Conference"
	const conferenceTicket uint = 100
	var remainingTickets uint = 100

	fmt.Printf("Welcome to %v booking application\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v are still available\n", conferenceTicket, remainingTickets)
	fmt.Println("----------------------------------")

	var firstName string
	var lastName string
	var email string
	var userTickets uint

	fmt.Print("enter your first name\t: ")
	//fmt.Scan(firstName) <- if u use this, then u input empty string and press enter, it will not wait for input
	fmt.Scan(&firstName) // <- use pointer to get the value

	fmt.Print("enter your last name\t: ")
	fmt.Scan(&lastName)

	fmt.Print("enter your email\t: ")
	fmt.Scan(&email)

	fmt.Print("enter number of tickets\t: ")
	fmt.Scan(&userTickets)

	remainingTickets -= userTickets

	fmt.Println("----------------------------------")
	fmt.Println("Thank you for booking", userTickets, "tickets", firstName, lastName, "your tickets will be sent to", email)
	fmt.Println("We have total of", conferenceTicket, "tickets and", remainingTickets, "are still available")
}
