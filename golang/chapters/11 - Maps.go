package main

import (
	"fmt"
	"strconv"
)

/**
 * Pada Array atau slice,
 * untuk mengakses data kita harus mengetahui index dari data tersebut.
 * Sedangkan pada map,
 * kita bisa mengakses data dengan menggunakan key dari data tersebut.
 * nama key pada map harus bersifat unik.
 */

func main() {
	// cara pertama membuat map
	//var userData = map[string]string, 0{
	//	"firstName":       "John",
	//	"lastName":        "Doe",
	//	"email":           "jhon@example.com",
	//	"numberOfTickets": "100",
	//}

	// cara kedua membuat map
	var userData = make(map[string]string)
	userData["firstName"] = "John"
	userData["lastName"] = "Doe"
	userData["email"] = "jhon@example.com"
	userData["numberOfTickets"] = "100"

	// memanggil data map
	fmt.Println(userData["firstName"])
	fmt.Println(userData["lastName"])

	// string to uint
	var numberOfTickets, _ = strconv.ParseUint(userData["numberOfTickets"], 10, 32)
	fmt.Printf("Number of tickets type: %T", numberOfTickets)
}
