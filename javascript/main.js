/**
 * LEARN CONSOLE
 * 
 * example: log, error, warn, assert, dir, clear
 * 
 * for more info please visit: 
 * https://developer.mozilla.org/en-US/docs/Web/API/console#instance_methods
 */

// console.log("Hello World")              // basic print to console
// console.error("This is an error")       // print the error message
// console.warn("This is a warning")       // print the warning message
// console.assert(1 === 2, "Not Equal")    // print the message if the condition is false
// console.dir(document)                   // print the object in the console
// console.clear()                         // clear the console


/**
 * LEARN VARIABLES
 *  
 * let      - can be reassigned
 * const    - cannot be reassigned
 */

// let age = 23

// console.log(age)

// age = 24
// console.log(age)

// const age = 23
// console.log(age)


/**
 * LEARN PRIMITIVE DATA TYPES
 * 
 **/

// const name = "John"
// const age = 30
// const rating = 4.5
// const isCool = true
// const y = undefined
// let z

// console.log(typeof z)


/**
 * LEARN STRINGS
 */

// const name = "John"
// const age = 30

// // Concatenation
// console.log("My name is " + name + " and I am " + age)

// // Template String
// console.log(`My name is ${name} and I am ${age}`)


/**
 * LEARN STRING METHODS
 * 
 * example: .length, .toUpperCase(), .toLowerCase(), 
 *          .substring(), .slice(), .split(), .replace(), .includes()
 * 
 * for more info please visit:
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String
 */

// const s = "Hello, World"
// console.log("lenght of string is " + s.length)                  
// console.log("string uppercase "    + s.toUpperCase())           
// console.log("string lowercase "    + s.toLowerCase())          
// console.log("print string until idx 3 " + s.substring(0, 3))    
// console.log("slice string " + s.slice())                
// console.log("split string with , (comma) " + s.split(','))                                       
// console.log("replace , (comma) with empty string " + s.replace(',', ''))        
// console.log("Hello, World include hello? " + s.includes('hello'))           


/**
 * LEARN ARRAYS
 * 
 * example: .push(), .pop(), .unshift(), 
 *          .shift(), .indexOf(), .slice(), 
 *          .splice(), .concat(), .sort(), .reverse()
 */

// const numbers = new Array(1, 2, 3, 4, 5, 6, 7, "Hello World")
// console.log(numbers)
// console.log(typeof numbers)
// const fruits = ["apples", "oranges", "pears", 10, true]
// console.log(fruits)
// console.log(fruits[1])

// fruits[3] = "grapes"
// console.log(fruits)

// fruits.push("mangos")
// console.log(fruits)
// fruits.unshift("strawberries")
// console.log(fruits)
// fruits.pop()

// console.log(Array.isArray(fruits))
// console.log(fruits.indexOf("oranges"))
// console.log(fruits)


/**
 * LEARN OBJECT LITERALS
 *
 * for more info please visit:
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object
 **/

const person = {
    firstName: "John",
    lastName: "Doe",
    age: 30,
    hobbies: ["music", "movies", "sports"],
    address: {
        street: "50 Main st",
        city: "Boston",
        state: "MA"
    }
}

// console.log(person)
// console.log(person.firstName, person.hobbies[1])                // get specific value
// const { firstName, lastName, address: { city } } = person       // destructuring
// console.log(firstName, lastName, city)
// // add property
// person.email = "maulanaakurniaa@yahoo.com"
// console.log(person)


/**
 * LEARN ARRAY OF OBJECTS
 * 
 * for more info please visit:
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object
 */

const todos = [
    {
        id: 1,
        text: "Take out trash",
        isCompleted: true
    },  
    {
        id: 2,
        text: "Meeting with boss",
        isCompleted: true
    },
    {
        id: 3,
        text: "Dentist appt",
        isCompleted: false
    }
]

// console.log(todos)

// const todoJSON = JSON.stringify(todos)
// console.log(todoJSON)


/**
 * LEARN LOOPS
 * 
**/

// // Basic Loops
// for (let i = 0; i < 10; i++) {
//     console.log(`For Loop Number: ${i}`)
// }

// // loop with while
// let i = 0
// while (i < 10) {
//     console.log(`While Loop Number: ${i}`)
//     i++
// }

// // loop with for of
// for (let todo of todos) {
//     console.log(todo.text)
// }

// // loop with forEach, map, filter
// todos.forEach(function(todo) {
//     console.log(todo.text)
// })

// const todoText = todos.map(function(todo) {
//     return todo.text
// })

// console.log(todoText)

// const todoCompleted = todos.filter(function(todo) {
//     return todo.isCompleted === true
// }).map(function(todo) {
//     return todo.text
// })
// console.log(todoCompleted)


/**
 * LEARN CONDITIONALS
 * 
 * example: ==, ===, !=, !==, >, <, >=, <=, ? (ternary operator)
 * 
 * for more info please visit:
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Comparison_Operators
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Logical_Operators
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Conditional_Operator
**/

const x = 20
const y = 15
// if (x === '20') {
//     console.log("x is same")
// }
// 
// 
// multiple condition
// if (x === 10) {
//     console.log("x is 10")
// } else if (x > 10) {
//     console.log("x is greater than 10")
// } else {
//     console.log("x is less than 10")
// }
// 
// with logical operator
// if (x > 5 || y > 10) {
//     console.log("x is more than 5 or y is more than 10")
// } 
// 

// TENARY
// const color = x > 10 ? "red" : "blue"
// console.log(color)

// SWITCH
// const color = "red"
// switch(color) {
//     case "red":
//         console.log("color is red")
//         break
//     case "blue":
//         console.log("color is blue")
//         break
//     default:
//         console.log("color is not red or blue")
// }


/**
 * LEARN FUNCTIONS
 * 
 * for more info please visit:
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions
 */

// function addNums(num1 = 1, num2 = 1) {
//     return num1 + num2
// }
// console.log(addNums())

// ARROW FUNCTION
// const addNums = (num1 = 1, num2 = 1) => num1 + num2
// console.log(addNums(5, 5))

// ARROW FUNCTION WITH OBJECT
// const addNums = (num1 = 1, num2 = 1) => ({ num1: num1, num2: num2 })
// console.log(addNums(5, 5))

// ARROW FUNCTION WITH MULTIPLE LINES
// const addNums = (num1 = 1, num2 = 1) => {
//     return num1 + num2
// }
// console.log(addNums(5, 5))

// CONSTRUCTOR FUNCTION
// function Person(firstName, lastName, dob) {
//     this.firstName = firstName
//     this.lastName = lastName
//     this.dob = new Date(dob)
//     this.getBirthYear = function() {
//         return this.dob.getFullYear()
//     }
//     this.getFullName = function() {
//         return `${this.firstName} ${this.lastName}`
//     }
// }

// INSTANTIATE OBJECT
// const person1 = new Person("John", "Doe", "4-3-1980")
// const person2 = new Person("Mary", "Smith", "3-6-1970")

// console.log(person1)
// console.log(person2)

// PROTOTYPE
// Person.prototype.getBirthYear = function() {
//    return this.dob.getFullYear()
// }
// Person.prototype.getFullName = function() {
//     return `${this.firstName} ${this.lastName}`
// }
// console.log(person1.getBirthYear())
// console.log(person2.getFullName())

// CLASS
// class Person {
//     constructor(firstName, lastName, dob) {
//         this.firstName = firstName
//         this.lastName = lastName
//         this.dob = new Date(dob)
//     }
//     getBirthYear() {
//         return this.dob.getFullYear()
//     }
//     getFullName() {
//         return `${this.firstName} ${this.lastName}`
//     }
// }

// INSTANTIATE OBJECT
// const person1 = new Person("John", "Doe", "4-3-1980")
// const person2 = new Person("Mary", "Smith", "3-6-1970")

// console.log(person1)
// console.log(person2)



/**
 * LEARN DOM
 * 
 * for more info please visit:
 * https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model
 * 
**/

// - get single element

// with getElementById
// console.group("get el with getElementById")
// console.log(document.getElementById("my-form"))
// console.groupEnd()

// with querySelector
// console.group("get el with querySelector")
// console.log(document.querySelector(".btn"))
// console.groupEnd()

// - get multiple element

// with getElementsByClassName
// console.group("get els with getElementsByClassName")
// console.log(document.getElementsByClassName("item"))
// console.groupEnd()

// with getElementsByTagName
// console.group("get els with getElementsByTagName")
// console.log(document.getElementsByTagName("li"))
// console.groupEnd()

// with querySelectorAll
// console.group("get els with querySelectorAll")
// console.log(document.querySelectorAll(".item"))
// console.groupEnd()

// - loops through the elements
// const items = document.querySelectorAll(".item")
// items.forEach((item) => console.log(item))

/**
 * MANIPULATE DOM
 * 
 * for more info please visit:
 * https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model
 */

// - change the style
// const ul = document.querySelector(".item")
// ul.firstElementChild.textContent = "Hello"
// ul.children[1].innerText = "Brad"
// ul.lastElementChild.innerHTML = "<h1>Hello</h1>"
// ul.lastElementChild.style.color = "red"


/**
 * EVENTS
 * 
 * for more info please visit:
 * https://developer.mozilla.org/en-US/docs/Web/Events
**/

// const btn = document.querySelector(".btn")

// click
// btn.addEventListener("click", (e) => {
//     e.preventDefault()
//     console.log(e.target.className)
//     document.querySelector("#my-form").style.background = "#ccc"
//     document.querySelector("body").classList.add("bg-dark")
//     document.querySelector(".item").lastElementChild.innerHTML = "<h1>Hello</h1>"
// })

// mouse event
// btn.addEventListener("mouseover", (e) => {
//     e.preventDefault()
//     btn.style.background = "red"
// })

// keyboard event
// document.querySelector("#name").addEventListener("input", (e) => {
//     console.log(e.target.value)
// })

// form event
const myForm = document.querySelector("#my-form")
const nameInput = document.querySelector("#name")
const emailInput = document.querySelector("#email")
const msg = document.querySelector(".msg")
const userList = document.querySelector("#users")

myForm.addEventListener("submit", (e) => {
    e.preventDefault()
    if(nameInput.value === "" || emailInput.value === "") {
        msg.classList.add("error")
        msg.innerHTML = "Please enter all fields"
        setTimeout(() => msg.remove(), 3000)
    } else {
        const li = document.createElement("li")
        li.appendChild(document.createTextNode(`${nameInput.value} : ${emailInput.value}`))
        userList.appendChild(li)
        nameInput.value = ""
        emailInput.value = ""
    }
})












